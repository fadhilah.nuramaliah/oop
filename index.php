<?php

require_once('animal.php'); //untuk ambil data di animal.php 
require('Ape.php'); 
require('Frog.php'); 

$sheep = new Animal("shaun");
echo "Name         : $sheep->name<br>"; 
echo "Legs         : $sheep->legs<br>"; 
echo "cold blooded : $sheep->cold_blooded<br><br>"; 
$sungokong = new Ape("kera sakti");
echo "Name         : $sungokong->name. <br>"; 
echo "Legs         : $sungokong->legs. <br>"; 
echo "cold blooded : $sungokong->cold_blooded. <br>"; 
$sungokong->yell(); 

$kodok = new Frog("buduk");
echo "<br><br>Name : $kodok->name<br>"; 
echo "Legs         : $kodok->legs<br>"; 
echo "cold blooded : $kodok->cold_blooded<br>"; 
$kodok->jump();
?>